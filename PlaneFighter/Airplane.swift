//
//  Airplane.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/1/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import Foundation
import SpriteKit

class Airplane: GameObject {
    var firingOffset: CGPoint = CGPoint.zero // 0,1 = front
    weak var weaponSetupDelegate: WeaponSetupDelegate?
    
    var weaponSystems: [WeaponSystem]!
    
//    convenience override init(texture: SKTexture?, color: UIColor, size: CGSize) {
//        self.init(startingPosition: CGPoint.zero, imageNamed: nil)
//    }
    
    convenience init(imageNamed: String) {
        let image = UIImage(named: imageNamed)
        self.init(startingPosition: CGPoint.zero, image: image!)
    }
    
    init(startingPosition: CGPoint = CGPoint.zero, image: UIImage) {
        let texture = SKTexture(image: image)
        super.init(texture: texture, color: .white, size: texture.size().scaleKeepingAspectRatio(maxValue: 80))
        self.name = kAirplaneName
        self.collisionCategory = .airplane
        self.collisionTarget = .airplane
        setupPhysics()
        self.attributes.configureHealth(to: 60)
        self.attributes.contactDamage = 100
        self.direction = .up
        self.velocity = 70.0
        self.position = startingPosition
        self.firingOffset = CGPoint(x: 0, y: self.size.height / 2)
        self.weaponSystems = []
    }
    
    open func setupWeaponSystems() {
        if let weaponSetupDelegate = weaponSetupDelegate {
            weaponSetupDelegate.setupWeaponSystems()
        } else {
            let system = WeaponSystem(for: self)
            system.collisionTarget = .airplaneEnemy
            let group = WeaponGroup(in: system, cooldown: 0.2)
            _ = Weapon(in: group, type: .ballistic)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        weaponSystems = nil
    }
    
    override func destroy() {
        weaponSystems = nil
        super.destroy()
    }
    
    func reset() {
        self.direction = .up
        self.velocity = 70.0
        self.size = CGSize(width: 80, height: 80)
        self.attributes.configureHealth(to: 60)
        self.attributes.contactDamage = 100
    }
}

protocol WeaponSetupDelegate: class {
    func setupWeaponSystems()
}
