//
//  BasicEnemy.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/4/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import Foundation
import SpriteKit

class BasicEnemy: Airplane {
    weak var enemyController: EnemyController!
    weak var target: Airplane?
    private var difficultyLevel: Int = 0 {
        didSet {
            computeLevel()
        }
    }
    
    init(_ enemyController: EnemyController, startingPosition: CGPoint, enemyLevel: Int = 0) {
        self.difficultyLevel = enemyLevel
        self.enemyController = enemyController
        let image = UIImage(named: "AlienShuttle")
        super.init(startingPosition: startingPosition, image: image!)
        self.direction = .down
        self.velocity = 70.0
        self.setupPhysics()
        self.collisionCategory = .airplaneEnemy
        self.computeLevel()
        self.setupWeaponSystems()
    }
    
    override func setupWeaponSystems() {
        var groupCooldown = 3.0 - TimeInterval(difficultyLevel) * 0.1
        groupCooldown = max(groupCooldown, 0.6)
        weaponSystems = []
        let system = WeaponSystem(for: self)
        system.collisionTarget = .airplanePlayer
        let group = WeaponGroup(in: system, cooldown: groupCooldown)
        let weapon = Weapon(in: group, type: .ballistic)
        weapon.color = .red
        weapon.velocity = 300
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        self.enemyController = nil
        self.target = nil
    }
    
    func computeLevel() {
        self.velocity = 70.0 + CGFloat(difficultyLevel) * 5
        self.velocity = min(self.velocity, 300)
    }
    
    override func destroy() {
        enemyController = nil
        super.destroy()
    }
}
