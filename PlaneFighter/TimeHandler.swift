//
//  TimeHandler.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/1/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import Foundation

struct TimeHandler {
    static var shared = TimeHandler()
    private var previousTime: TimeInterval = 0
    private var currentTime: TimeInterval = 0 {
        willSet {
            previousTime = self.currentTime
        }
        didSet {
            if self.previousTime != 0 {
                NotificationCenter.default.post(name: TimerUpdatedNotificationName, object: nil)
            }
        }
    }
    private var elapsedTime: TimeInterval {
        return currentTime - previousTime
    }
    
    //Convenience
    
    static var current: TimeInterval {
        set {
            shared.currentTime = newValue
        }
        get {
            return shared.currentTime
        }
    }
    
    static var elapsed: TimeInterval {
        get {
            return shared.elapsedTime
        }
    }
    
    static var isReady: Bool {
        get {
            return shared.previousTime != 0
        }
    }
}
