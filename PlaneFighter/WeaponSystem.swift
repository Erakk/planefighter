//
//  WeaponSystem.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/8/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import SpriteKit

class WeaponSystem {
    var weaponGroups: [WeaponGroup]!
    weak var airplane: Airplane?
    var collisionTarget: CollisionCategory = .airplane
    
    init(for airplane: Airplane) {
        self.airplane = airplane
        self.weaponGroups = []
        self.airplane?.weaponSystems.append(self)
    }
}
