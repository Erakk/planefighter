//
//  MissileNode.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/1/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import UIKit
import SpriteKit

class Missile: GameObject {
    weak var airplane: Airplane!
    
    init(withImage imageNamed: String, firedFrom origin: Airplane) {
        let image = UIImage(named: imageNamed)
        let texture = SKTexture(image: image!)
        super.init(texture: texture, color: .white, size: texture.size().scaleKeepingAspectRatio(maxValue: 20))
        self.airplane = origin
        self.name = kMissileName
        self.collisionCategory = .missile
        self.collisionTarget = .airplane
        setupPhysics()
        self.velocity = 100.0
        self.direction = origin.direction
        self.attributes.configureHealth(to: 1)
        self.attributes.contactDamage = 1
        
        
        //Auto destroy
        let wait = SKAction.wait(forDuration: 5)
        let destroy = SKAction.run(self.destroy)
        let autoDestroy = SKAction.sequence([wait, destroy])
        self.run(autoDestroy)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func destroy() {
        airplane = nil
        super.destroy()
    }
}
