//
//  GameScene.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/1/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    fileprivate var label: SKLabelNode?
    var player: Player!
    var enemyController: EnemyController!
    
    override func didMove(to view: SKView) {
        setupLabel()
        setupPlayer()
        setupEnemyController()
        self.physicsWorld.contactDelegate = self
    }
    
    func setupPlayer() {
        player = Player(with: self)
    }
    
    func setupEnemyController() {
        enemyController = EnemyController(with: self)
    }
    
    func setupLabel() {
        label = childNode(withName: "//positionLabel") as? SKLabelNode
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        TimeHandler.current = currentTime
        guard TimeHandler.isReady else {
            return
        }
        
        player.updateAirplanePosition()
        updateObjectsPotisions()
        
        label?.text = player.healthStatus()
    }
    
    func updateObjectsPotisions() {
        enumerateChildNodes(withName: "//*") { (node, _) in
            if let node = node as? GameObject {
                if node.objectId != self.player.airplane.objectId && node is Airplane {
                    if !self.frame.intersects(node.frame) {
                        node.destroy()
                    }
                }
            }
        }
    }
}

// MARK - Contact Delegate
extension GameScene: SKPhysicsContactDelegate {
    func didBegin(_ contact: SKPhysicsContact) {
        if let missile = contact.bodyB.node as? Missile, let airplane = contact.bodyA.node as? Airplane {
            computeContact(between: missile, and: airplane, at: contact.contactPoint)
        } else if let missile = contact.bodyA.node as? Missile, let airplane = contact.bodyB.node as? Airplane {
            computeContact(between: missile, and: airplane, at: contact.contactPoint)
        }
    }
    
    func computeContact(between missile: Missile, and airplane: Airplane, at contactPoint: CGPoint? = nil) {
        if let contactPoint = contactPoint {
            createContactAnimation(at: contactPoint)
        }
        if missile.airplane != airplane && !airplane.isDestroyed { //missile was not fired by contact airplane
            missile.damage(target: airplane)
            if missile.needsToBeDestroyed {
                missile.destroy()
            }
            var playerAnihilated = false
            if airplane.needsToBeDestroyed {
                playerAnihilated = airplane == player.airplane
                let explosionParticle = ParticleFactory.explosionParticle()
                explosionParticle.targetNode = scene
                explosionParticle.position = airplane.position
                addChild(explosionParticle)
                
                airplane.destroyAnimated()
            }
            
            if playerAnihilated {
                displayDeathMessageAndResetGame()
            }
        }
    }
    
    func createContactAnimation(at point: CGPoint) {
        let explosion = SKShapeNode(circleOfRadius: 5.0)
        explosion.strokeColor = .gray
        explosion.lineWidth = 3.4
        explosion.position = point
        addChild(explosion)
        let blowUp = SKAction.scale(to: 10.0, duration: 0.4)
        let fadeOut = SKAction.fadeOut(withDuration: 0.4)
        
        explosion.run(blowUp)
        explosion.run(fadeOut, completion: explosion.removeFromParent)
    }
    
    func displayDeathMessageAndResetGame() {
        let dead = SKLabelNode(fontNamed: "Chalkduster")
        dead.text = "You Died"
        dead.fontSize = 65
        dead.fontColor = SKColor.red
        dead.position = CGPoint(x: frame.midX, y: frame.midY)
        
        addChild(dead)
        
        let increaseSize = SKAction.scale(to: 2.0, duration: 3)
        let fadeOut = SKAction.fadeOut(withDuration: 2)
        let resetGame = SKAction.run(self.resetGame)
        
        let sequence = SKAction.sequence([increaseSize, fadeOut, resetGame])
        
        dead.run(sequence)
    }
    
    func resetGame() {
        player = nil
        enemyController = nil
        enumerateChildNodes(withName: "//*") { (node, _) in
            if let node = node as? GameObject {
                node.destroy()
            }
        }
        setupPlayer()
        setupEnemyController()
    }
}

// MARK - touch handling
extension GameScene {
    func touchDown(atPoint pos : CGPoint) {
        player.lastTouch = pos
    }
    
    func touchMoved(toPoint pos : CGPoint) {
        player.newTouch(pos)
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let touch = touches.first {
            let pos = touch.location(in: self)
            touchDown(atPoint: pos)
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        if let touch = touches.first {
            let pos = touch.location(in: self)
            touchMoved(toPoint: pos)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        if let touch = touches.first {
            let pos = touch.location(in: self)
            touchUp(atPoint: pos)
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        if let touch = touches.first {
            let pos = touch.location(in: self)
            touchUp(atPoint: pos)
        }
    }
}
