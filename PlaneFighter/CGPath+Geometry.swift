//
//  CGPath+Geometry.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/8/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import SpriteKit

/*
 * The front of the object is primarily facing right, as SpriteKit considers 0 degrees/radians to be facing east.
 */

extension CGPath {
    class func ellipse(from size: CGSize) -> CGPath {
        let reorientedSize = CGSize(width: size.height, height: size.width)
        let rect = CGRect(origin: CGPoint(x:  -reorientedSize.halfHeight, y: -reorientedSize.halfHeight), size: reorientedSize)
        return CGPath(ellipseIn: rect, transform: nil)
    }
    
    class func triangle(from size: CGSize) -> CGPath {
        let trianglePath = CGMutablePath()
        let reorientedSize = CGSize(width: size.height, height: size.width)
        trianglePath.move(to: reorientedSize.middleRight)
        trianglePath.addLine(to: reorientedSize.bottomLeft)
        trianglePath.addLine(to: reorientedSize.topLeft)
        trianglePath.addLine(to: reorientedSize.middleRight)
        trianglePath.closeSubpath()
        
        return trianglePath
    }
}
