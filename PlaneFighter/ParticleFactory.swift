//
//  ParticleFactory.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/10/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import SpriteKit

class ParticleFactory {
    class func smokeParticle(attachedTo node: SKNode? = nil) -> SKEmitterNode {
        let particle = SKEmitterNode(fileNamed: "SmokeParticle")
        
        if let node = node {
            particle?.targetNode = node.parent
            node.addChild(particle!)
        }
        
        return particle!
    }
    
    class func explosionParticle(attachedTo node: SKNode? = nil) -> SKEmitterNode {
        let particle = SKEmitterNode(fileNamed: "ExplosionParticle")
        
        if let node = node {
            particle?.targetNode = node.parent
            node.addChild(particle!)
        }
        return particle!
    }
}
