//
//  Player.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/1/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import Foundation
import SpriteKit

class Player {
    var airplane: Airplane!
    var lastTouch: CGPoint?
    private var accumulatedMovement: CGPoint = CGPoint.zero
    private weak var scene: GameScene!
    
    init(with gameScene: GameScene) {
        self.scene = gameScene
        let image = UIImage(named: "F22Fighter")
        airplane = Airplane(image: image!)
        airplane.weaponSetupDelegate = self
        airplane.setupWeaponSystems()
        airplane.direction = .up
//        airplane.attributes.configureHealth(to: 200) //this is for testing, let it die quickly.
        airplane.attributes.configureHealth(to: 60)
        airplane.setupPhysics()
        airplane.collisionCategory = .airplanePlayer
        airplane.physicsBody?.velocity = CGVector.zero
        scene.addChild(airplane)
        
//        rotateForever()
    }
    
    func rotateForever() {
        let rotate = SKAction.rotate(byAngle: CGFloat.pi, duration: 2)
        let forever = SKAction.repeatForever(rotate)
        airplane.run(forever)
    }
    
    deinit {
        airplane = nil
        scene = nil
    }
    
    func newTouch(_ touch: CGPoint) {
        if !airplane.isDestroyed {
            let movedBy = lastTouch! - touch
            accumulatedMovement -= movedBy
            lastTouch = touch
        }
    }
    
    func updateAirplanePosition() {
        if !airplane.isDestroyed {
            airplane.move(to: airplane.position + accumulatedMovement)
            accumulatedMovement = CGPoint.zero
        }
    }
    
    func healthStatus() -> String {
        return "\(airplane.attributes.currentHealth)/\(airplane.attributes.maximumHealth)"
    }
    
    func reset() {
        airplane.reset()
        airplane.attributes.configureHealth(to: 200)
        scene.addChild(airplane)
    }
}

extension Player: WeaponSetupDelegate {
    func setupWeaponSystems() {
        let system = WeaponSystem(for: airplane)
        system.collisionTarget = .airplaneEnemy
        let group = WeaponGroup(in: system, cooldown: 0.2)
        let weapon1 = Weapon(in: group, type: .ballistic)
        weapon1.firingOffset = airplane.size.middleRight
//        let weapon2 = Weapon(in: group, type: .ballistic)
//        weapon2.firingOffset = airplane.size.bottomLeft
//        let weapon3 = Weapon(in: group, type: .ballistic)
//        weapon3.firingOffset = airplane.size.topLeft
//        let group = WeaponGroup(in: system, cooldown: 0.5)
//        let weapon2 = Weapon(in: group, type: .missile)
//        weapon2.firingOffset = airplane.size.bottomLeft
//        let weapon3 = Weapon(in: group, type: .missile)
//        weapon3.firingOffset = airplane.size.topLeft
    }
}
