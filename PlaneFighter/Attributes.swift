//
//  Attributes.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/4/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import SpriteKit

class Attributes {
    var indestructible: Bool = false
    var maximumHealth: Int = 0 {
        didSet {
            if maximumHealth < 0 {
                maximumHealth = 0
            }
        }
    }
    var currentHealth: Int = 0 {
        didSet {
            if currentHealth > maximumHealth {
                currentHealth = maximumHealth
            } else if currentHealth < 0 {
                currentHealth = 0
            }
        }
    }
    var contactDamage: Int = 0
    
    func setHealth(to percent: Float) {
        currentHealth = Int(Float(maximumHealth) * percent)
    }
    
    func inflictDamage(_ ammount: Int) -> Bool {
        if !indestructible {
            currentHealth -= ammount
        }
        return currentHealth == 0
    }
    
    func healToFull() {
        currentHealth = maximumHealth
    }
    
    func configureHealth(to value: Int) {
        maximumHealth = value
        currentHealth = value
    }
}
