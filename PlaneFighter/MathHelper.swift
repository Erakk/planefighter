//
//  MathHelper.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/1/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import SpriteKit

extension CGPoint {
    static func +( left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x + right.x, y: left.y + right.y)
    }
    
    static func -( left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x - right.x, y: left.y - right.y)
    }
    
    static func +=( left: inout CGPoint, right: CGPoint) {
        left = left + right
    }
    
    static func -=( left: inout CGPoint, right: CGPoint) {
        left = left - right
    }
    
    static func *( left: CGPoint, right: CGFloat) -> CGPoint {
        return CGPoint(x: left.x * CGFloat(right), y: left.y * CGFloat(right))
    }
    
    mutating func max(_ limit: CGFloat) {
        if self.x > limit {
            self.x = limit
        }
        if self.y > limit {
            self.y = limit
        }
    }
    
    var vector: CGVector {
        return CGVector(dx: x, dy: y)
    }
}

extension CGVector {
    var point: CGPoint {
        return CGPoint(x: dx, y: dy)
    }
    static var one: CGVector {
        return CGVector(dx: 1.0, dy: 1.0)
    }
    static var up: CGVector {
        return CGVector(dx: 0, dy: 1.0)
    }
    static var down: CGVector {
        return CGVector(dx: 0, dy: -1.0)
    }
    static var left: CGVector {
        return CGVector(dx: -1.0, dy: 0)
    }
    static var right: CGVector {
        return CGVector(dx: 1.0, dy: 0)
    }
    
    static func +( left: CGVector, right: CGVector) -> CGVector {
        return CGVector(dx: left.dx + right.dx, dy: left.dy + right.dy)
    }
    
    static func -( left: CGVector, right: CGVector) -> CGVector {
        return CGVector(dx: left.dx - right.dx, dy: left.dy - right.dy)
    }
    
    static func +=( left: inout CGVector, right: CGVector) {
        left = left + right
    }
    
    static func -=( left: inout CGVector, right: CGVector) {
        left = left - right
    }
    
    static func *( left: CGVector, right: CGFloat) -> CGVector {
        return CGVector(dx: left.dx * right, dy: left.dy * right)
    }
    
    static func *=( left: inout CGVector, right: CGFloat) {
        left = left * right
    }
    
    static func /( left: CGVector, right: CGFloat) -> CGVector {
        return CGVector(dx: left.dx / right, dy: left.dy / right)
    }
    
    static func /=( left: inout CGVector, right: CGFloat) {
        left = left / right
    }
    
    var rotationInDegrees: CGFloat {
        let radians = rotationInRadians
        return radians.toDegrees
    }
    
    var rotationInRadians: CGFloat {
        return atan2(dy, dx)
    }
    
    var magnitude: CGFloat {
        return distance(to: CGVector.zero)
    }
    
    func distance(to vector: CGVector) -> CGFloat {
        var deltaX = self.dx - vector.dx
        var deltaY = self.dy - vector.dy
        deltaX *= deltaX
        deltaY *= deltaY
        
        return sqrt(deltaX + deltaY)
    }
    
    mutating func rotate(by degrees: CGFloat) {
        let baseDegrees = self.rotationInDegrees
        let magnitude = self.magnitude
        self = magnitude.vectorWithDegrees(baseDegrees + degrees)
    }
    
    mutating func rotate(to degrees: CGFloat) {
        let magnitude = self.magnitude
        self = magnitude.vectorWithDegrees(degrees)
    }
    
    mutating func normalizeZero() {
        self.dx.normalizeZero()
        self.dy.normalizeZero()
    }
    
    mutating func normalizeOne() {
        let highest = max(abs(self.dx), abs(self.dy))
        self.dx /= highest
        self.dy /= highest
    }
}

extension CGFloat {
    func format(f: String) -> String {
        return String(format: "%\(f)f", self)
    }
    
    mutating func normalizeZero() {
        if self < 0.001 && self > -0.001 {
            self = CGFloat(0)
        }
    }
    
    var toRadians: CGFloat {
        return self * CGFloat.pi / 180
    }
    
    var toDegrees: CGFloat {
        return self * 180 / CGFloat.pi
    }
    
    var vectorFromDegrees: CGVector {
        var dx = 1 * cos(self.toRadians)
        var dy = 1 * sin(self.toRadians)
        dx.normalizeZero()
        dy.normalizeZero()
        return CGVector(dx: dx, dy: dy)
    }
    
    var vectorFromRadians: CGVector {
        var dx = 1 * cos(self)
        var dy = 1 * sin(self)
        dx.normalizeZero()
        dy.normalizeZero()
        return CGVector(dx: dx, dy: dy)
    }
    
    func vectorWithDegrees(_ degrees: CGFloat) -> CGVector {
        var dx = self * cos(degrees.toRadians)
        var dy = self * sin(degrees.toRadians)
        dx.normalizeZero()
        dy.normalizeZero()
        return CGVector(dx: dx, dy: dy)
    }
    
    func vectorWithModule(_ module: CGFloat) -> CGVector {
        var dx = module * cos(self.toRadians)
        var dy = module * sin(self.toRadians)
        dy.normalizeZero()
        dx.normalizeZero()
        return CGVector(dx: dx, dy: dy)
    }
}

//convenience
extension CGSize {
    func scaleKeepingAspectRatio(maxValue: CGFloat) -> CGSize {
        let sideMaximum = max(self.width, self.height)
        let ratio = maxValue / sideMaximum
        return CGSize(width: self.width * ratio, height: self.height * ratio)
    }
    
    var halfWidth: CGFloat {
        return self.width / 2
    }
    var halfHeight: CGFloat {
        return self.height / 2
    }
    
    var middleTop: CGPoint {
        return CGPoint(x: 0, y: halfHeight)
    }
    var middleBottom: CGPoint {
        return CGPoint(x: 0, y: -halfHeight)
    }
    var middleLeft: CGPoint {
        return CGPoint(x: -halfWidth, y: 0)
    }
    var middleRight: CGPoint {
        return CGPoint(x: halfWidth, y: 0)
    }
    
    var topLeft: CGPoint {
        return CGPoint(x: -halfWidth, y: halfHeight)
    }
    var topRight: CGPoint {
        return CGPoint(x: halfWidth, y: halfHeight)
    }
    var bottomLeft: CGPoint {
        return CGPoint(x: -halfWidth, y: -halfHeight)
    }
    var bottomRight: CGPoint {
        return CGPoint(x: halfWidth, y: -halfHeight)
    }
}

