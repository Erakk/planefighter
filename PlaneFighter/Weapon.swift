//
//  Weapon.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/8/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import SpriteKit

class Weapon {
    var firingOffset: CGPoint
    var type: WeaponType
    var damage: Int
    var color: UIColor?
    var velocity: CGFloat = 0
    weak var weaponGroup: WeaponGroup?
    
    init(in weaponGroup: WeaponGroup, type: WeaponType = .ballistic, offset: CGPoint = CGPoint.zero, damage: Int = 0) {
        self.weaponGroup = weaponGroup
        self.type = type
        self.firingOffset = offset
        self.damage = damage
        self.weaponGroup?.weapons.append(self)
    }
    
    func fire() {
        if let gameScene = weaponGroup?.weaponSystem?.airplane?.scene {
            if let missile = generateMissile(), let collisionTarget = weaponGroup?.weaponSystem?.collisionTarget {
                missile.collisionTarget = collisionTarget
                gameScene.addChild(missile)
            }
        }
    }
    
    fileprivate func generateMissile() -> Missile? {
        switch type {
        case .ballistic:
            return generateBallisticTypeMissile()
        case .missile:
            return generateMissileTypeMissile()
        case .laser:
            return generateLaserTypeMissile()
        }
    }
    
    fileprivate func calculateFiringPosition(for missile: Missile) {
        if let airplane  = weaponGroup?.weaponSystem?.airplane {
            let offsetVector = firingOffset.vector
            let offsetDegree = offsetVector.rotationInDegrees
            var frontVector = CGVector.right * offsetVector.magnitude
            frontVector.rotate(to: airplane.direction.rotationInDegrees + offsetDegree)
            missile.position = airplane.position + frontVector.point
        }
    }
}

extension Weapon {
    fileprivate func generateBallisticTypeMissile() -> Missile? {
        if let airplane  = weaponGroup?.weaponSystem?.airplane {
            let ballistic = Missile(withImage: "Bullet", firedFrom: airplane)
//            ballistic.size = CGSize(width: 5, height: 10)
            ballistic.velocity = velocity > 0 ? velocity : 1150.0
            ballistic.attributes.configureHealth(to: 5)
            ballistic.attributes.contactDamage = damage > 0 ? damage : 22
            ballistic.direction = airplane.direction
            calculateFiringPosition(for: ballistic)
            
            return ballistic
        }
        
        return nil
    }
    
    fileprivate func generateMissileTypeMissile() -> Missile? {
        if let airplane  = weaponGroup?.weaponSystem?.airplane {
            let missile = Missile(withImage: "Bullet", firedFrom: airplane)
            missile.velocity = velocity > 0 ? velocity : 300.0
//            missile.size = CGSize(width: 5, height: 20)
            missile.attributes.configureHealth(to: 5)
            missile.attributes.contactDamage = damage > 0 ? damage : 175
            missile.direction = airplane.direction
            calculateFiringPosition(for: missile)
            
            setupAcceleration(for: missile, maximumVelocity: 900, acceleration: 900, duration: 0.7)
            setupSmokeEmitter(for: missile)
            
            return missile
        }
        
        return nil
    }
    
    fileprivate func generateLaserTypeMissile() -> Missile? {
        if let airplane  = weaponGroup?.weaponSystem?.airplane {
            let laser = Missile(withImage: "Bullet", firedFrom: airplane)
            laser.velocity = 0
//            laser.size = CGSize(width: 5, height: 1200)
            laser.attributes.configureHealth(to: 5)
            laser.attributes.indestructible = true
            laser.attributes.contactDamage = damage > 0 ? damage : 65
            laser.direction = airplane.direction
            calculateFiringPosition(for: laser)
            
            return laser
        }
        
        return nil
    }
    
    //Helper
    private func setupAcceleration(for missile: Missile, maximumVelocity: CGFloat, acceleration: CGFloat, duration: TimeInterval) {
        let maximumVelocity: CGFloat = 900
        let steps = 10
        let stepDuration = duration / TimeInterval(steps)
        let deltaVelocity = (maximumVelocity - missile.velocity) / CGFloat(steps)
        let accelerate = SKAction.run {
            missile.velocity += deltaVelocity
        }
        let wait = SKAction.wait(forDuration: stepDuration)
        
        var actionSequence = [SKAction]()
        for _ in 0..<steps {
            actionSequence.append(accelerate)
            actionSequence.append(wait)
        }
        
        let sequence = SKAction.sequence(actionSequence)
        missile.run(sequence)
    }
    
    private func setupSmokeEmitter(for missile: Missile) {
        if let scene = missile.airplane.scene {
            let particle = ParticleFactory.smokeParticle()
            particle.targetNode = scene
            missile.addChild(particle)
        }
    }
    
}

enum WeaponType {
    case ballistic
    case missile
    case laser
}
