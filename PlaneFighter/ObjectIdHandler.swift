//
//  ObjectIdHandler.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/4/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import Foundation

final class ObjectIdHandler {
    private var counter: Int = 0
    static let shared = ObjectIdHandler()
    
    class func newId() -> Int {
        let newId = shared.counter
        shared.counter += 1
        return newId
    }
    
    class func lastId() -> Int {
        return shared.counter
    }
}
