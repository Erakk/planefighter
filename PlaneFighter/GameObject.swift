//
//  GameObject.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/1/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import Foundation
import SpriteKit

class GameObject: SKSpriteNode {
    let objectId: Int = ObjectIdHandler.newId()
    var isDestroyed: Bool = false
    var needsToBeDestroyed: Bool = false
    var attributes: Attributes = Attributes()
    
    var velocity: CGFloat = 10.0 {
        didSet {
            updateVelocity()
        }
    }
    var direction: CGVector {
        set {
            self.zRotation = newValue.rotationInRadians
            updateVelocity()
        }
        get {
            return self.zRotation.vectorFromRadians
        }
    }
    var collisionCategory: CollisionCategory = CollisionCategory.none {
        didSet {
            self.physicsBody?.categoryBitMask = self.collisionCategory.rawValue
        }
    }
    var collisionTarget: CollisionCategory = CollisionCategory.none {
        didSet {
            self.physicsBody?.contactTestBitMask = self.collisionTarget.rawValue
        }
    }
    
    
    
    override var texture: SKTexture? {
        didSet {
            setupPhysics()
        }
    }
    
    func setupPhysics() {
        if let texture = texture {
            self.physicsBody = SKPhysicsBody(texture: texture, size: self.size)
            self.physicsBody?.isDynamic = true
            //        self.physicsBody?.usesPreciseCollisionDetection = true
            self.physicsBody?.affectedByGravity = false
            self.physicsBody?.mass = 0
            self.physicsBody?.friction = 0
            self.physicsBody?.angularDamping = 0
            self.physicsBody?.linearDamping = 0
            self.physicsBody?.categoryBitMask = self.collisionCategory.rawValue
            self.physicsBody?.contactTestBitMask = self.collisionTarget.rawValue
            self.physicsBody?.collisionBitMask = CollisionCategory.none.rawValue
            self.physicsBody?.velocity = direction * velocity
        }
    }
    
    func destroyAnimated() {
        self.isDestroyed = true
        let destroy = SKAction.run(self.destroy)
        let animate = SKAction.scale(to: 0.2, duration: 0.5)
        let sequence = SKAction.sequence([animate, destroy])
        run(sequence)
    }
    
    func destroy () {
        self.isDestroyed = true
        self.physicsBody = nil
        self.removeFromParent()
    }
    
    private func updateVelocity() {
        if let physicsBody = self.physicsBody {
            physicsBody.velocity = direction * velocity
        }
    }
    
    func damage(target: GameObject, selfDamaged: Bool = true) {
        let targetDestroyed = target.attributes.inflictDamage(attributes.contactDamage)
        target.needsToBeDestroyed = targetDestroyed
        if selfDamaged {
            let selfDestroyed = attributes.inflictDamage(target.attributes.contactDamage)
            needsToBeDestroyed = selfDestroyed
        }
    }
}

// Direction
extension GameObject {
    func face(towards point: CGPoint) {
        var targetVector = point.vector - self.position.vector
        targetVector.normalizeOne()
        self.direction = targetVector
    }
}


// Movement
extension GameObject {
    func applyVelocity(for elapsedTime: TimeInterval = TimeHandler.elapsed) {
        move(dx: 0, dy: velocity * direction.dy, for: elapsedTime)
    }
    
    func move(dx: CGFloat, dy: CGFloat, for elapsedTime: TimeInterval = TimeHandler.elapsed) {
        let dxByTime = dx * CGFloat(elapsedTime)
        let dyByTime = dy *  CGFloat(elapsedTime)
        let velocityVector = CGVector(dx: dxByTime, dy: dyByTime)
        let reposition = SKAction.move(by: velocityVector, duration: elapsedTime)
        run(reposition)
    }
    
    func move(by vector: CGVector, for elapsedTime: TimeInterval = TimeHandler.elapsed) {
        move(dx: vector.dx * CGFloat(elapsedTime), dy: vector.dy * CGFloat(elapsedTime), for: elapsedTime)
    }
    
    func move(to point: CGPoint, for elapsedTime: TimeInterval = 0) {
        let reposition = SKAction.move(to: point, duration: elapsedTime)
        run(reposition)
    }
}

enum CollisionCategory: UInt32 {
    case none = 0
    case missile = 1
    case airplanePlayer = 2
    case airplaneEnemy = 4
    case airplane = 6
}
