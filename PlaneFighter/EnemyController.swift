//
//  EnemyController.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/1/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import Foundation
import SpriteKit

class EnemyController {
    var autoSpawn: Autofire!
    var autoDifficulty: Autofire!
    var difficultyLevel: Int = 0
    weak var scene: GameScene!
    
    init(with gameScene: GameScene) {
        self.scene = gameScene
        self.autoSpawn = Autofire()
        self.autoDifficulty = Autofire()
        
        self.autoSpawn.interval = 3.0
        self.autoSpawn.delegate = self
        
        self.autoDifficulty.interval = 5.0
        self.autoDifficulty.delegate = self
    }
    
    deinit {
        self.autoSpawn = nil
        self.autoDifficulty = nil
        self.scene = nil
    }
    
    func spawnEnemy(_ location: CGPoint = CGPoint.zero, offScreen: Bool = true, enemyType: EnemyType = .basic) {
        var spawnLocation = location
        
        let topPosition = abs(scene.frame.origin.y)
        let halfWidth = abs(scene.frame.origin.x)
        
        if spawnLocation == CGPoint.zero {
            let randomX = Int.random(lower: Int(-halfWidth), upper: Int(halfWidth))
            spawnLocation.x = CGFloat(randomX)
        }
        
        if offScreen {
            spawnLocation.y = topPosition
        }
        
        var enemy: BasicEnemy? = nil
        switch enemyType {
        case .basic:
            enemy = BasicEnemy(self, startingPosition: spawnLocation, enemyLevel: difficultyLevel)
            enemy?.direction = .down
        }
        enemy?.target = scene.player.airplane
        scene.addChild(enemy!)
    }
    
    func spawnRandomEnemy() {
        spawnEnemy()
    }
    
    func removeEnemy(with objectId: Int) {
        scene.enumerateChildNodes(withName: kAirplaneName) { (airplane, _) in
            if let basicEnemy = airplane as? BasicEnemy {
                if basicEnemy.objectId == objectId {
                    basicEnemy.destroy()
                }
            }
        }
    }
    
    func increaseDifficulty() {
        if autoSpawn.interval > 0.5 {
            autoSpawn.interval *= 0.9
        }
        difficultyLevel += 1
    }
}

extension EnemyController: Fireable {
    func autofireDidFire(_ autofire: Autofire) {
        guard let autoSpawn = autoSpawn, let autoDifficulty = autoDifficulty else {
            return
        }
        if autofire.objectId == autoSpawn.objectId {
            spawnRandomEnemy()
        } else if autofire.objectId == autoDifficulty.objectId {
            increaseDifficulty()
        }
    }
}

enum EnemyType {
    case basic
}
