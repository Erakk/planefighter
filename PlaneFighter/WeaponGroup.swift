//
//  WeaponGroup.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/8/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import SpriteKit

class WeaponGroup {
    var weapons: [Weapon]!
    var autofire: Autofire!
    weak var weaponSystem: WeaponSystem?
    
    init(in weaponSystem: WeaponSystem, cooldown: TimeInterval) {
        self.weaponSystem = weaponSystem
        self.weapons = []
        self.autofire = Autofire()
        self.autofire.interval = cooldown
        self.autofire.delegate = self
        self.weaponSystem?.weaponGroups.append(self)
    }
    
    fileprivate func fire() {
        for weapon in weapons {
            weapon.fire()
        }
    }
}

extension WeaponGroup: Fireable {
    func autofireDidFire(_ autofire: Autofire) {
        fire()
    }
}
