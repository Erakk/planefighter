//
//  AutofireHandler.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/1/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import Foundation

class Autofire {
    let objectId: Int = ObjectIdHandler.newId()
    weak var delegate: Fireable?
    var interval: TimeInterval = 0
    var isAutofireEnabled: Bool = true
    private var elapsed: TimeInterval = 0
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateFromTimer), name: TimerUpdatedNotificationName, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func fireActionIfPossible() {
        if isAutofireEnabled {
            if let delegate = delegate {
                delegate.autofireDidFire(self)
            }
        }
    }
    
    @objc private func updateFromTimer() {
        update(byAdding: TimeHandler.elapsed)
    }
    
    private func update(byAdding elapsed: TimeInterval) {
        self.elapsed += elapsed
        if self.elapsed >= interval {
            self.elapsed -= interval
            fireActionIfPossible()
        }
    }
}

protocol Fireable: class {
    func autofireDidFire(_ autofire: Autofire)
}
