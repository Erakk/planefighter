//
//  Constants.swift
//  PlaneFighter
//
//  Created by Calvin Gonçalves de Aquino on 8/1/17.
//  Copyright © 2017 Calvin. All rights reserved.
//

import Foundation

// Object names
let kMissileName = "Missile"
let kAirplaneName = "Airplane"

// Notification names
let TimerUpdatedNotificationName = Notification.Name("timerUpdated")
